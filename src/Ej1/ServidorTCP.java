package Ej1;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Scanner;

public class ServidorTCP {
    public static void main(String[] args) {

        //construir el objeto para que escuche por el puerto indicado
        ServerSocket servidor = null;
        try {
            servidor = new ServerSocket(55000);

            System.out.println("Aceptando conexiones...");
            //Espera bloqueado conexiones de clientes
            //Cuando llegue una devolverá el accept un Socket
            Socket cliente = servidor.accept();

            System.out.println("Conexión establecida!");

            //inicialización de los streams para entreda y salida de datos
            DataInputStream entrada = new DataInputStream(cliente.getInputStream());
            DataOutputStream salida = new DataOutputStream(cliente.getOutputStream());



            //Calculadora

            while(true) {
                System.out.println("Recibiendo operación a realizar:");
                int operation = Integer.parseInt(entrada.readUTF());

                if (operation == 5) {
                    break;
                }

                System.out.println("Recibiendo el primer número:");
                double x = Double.parseDouble(entrada.readUTF());
                System.out.println(x);

                System.out.println("Recibiendo el segundo número:");
                double y = Double.parseDouble(entrada.readUTF());
                System.out.println(y);

                double resultado = 0;

                switch (operation) {
                    case 1:
                        resultado = x + y;
                        System.out.println("El reslutado de la suma es = "+resultado);
                        salida.writeUTF(resultado+"");
                        break;
                    case 2:
                        resultado = x - y;
                        System.out.println("El reslutado de la resta es = "+resultado);
                        salida.writeUTF(resultado+"");
                        break;
                    case 3:
                        resultado = x * y;
                        System.out.println("El reslutado de la multiplicación es = "+resultado);
                        salida.writeUTF(resultado+"");
                        break;
                    case 4:
                        resultado = x / y;
                        System.out.println("El reslutado de la división es = "+resultado);
                        salida.writeUTF(resultado+"");
                        break;
                    case 5:
                        break;
                    default:
                        System.out.println("Non valid option");
                        break;
                }

                salida.writeUTF(resultado + "");
            }


            System.out.println("Cerrando Streams y sockets...");
            entrada.close();
            salida.close();
            cliente.close();
            servidor.close();
        } catch (IOException e) {
            System.err.println("Error al crear el socket");
            throw new RuntimeException(e);
        }
    }
}
