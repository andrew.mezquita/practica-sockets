package Ej1;

import java.io.*;
import java.net.Socket;
import java.util.Scanner;

public class ClienteTCP {
    private static int operation;
    private static double num1;
    private static double num2;
    public static void main(String[] args) {
        Scanner scC = new Scanner(System.in);
        Scanner scN = new Scanner(System.in);

        System.out.println("Creando y conectando el socket stream cliente");
        try {
            Socket cliente = new Socket("localhost",55000);

            //Stream de entrada de datos
            InputStream is = cliente.getInputStream();
            DataInputStream dis = new DataInputStream(is);
            //Stream de salida de datos
            OutputStream os = cliente.getOutputStream();
            DataOutputStream dos = new DataOutputStream(os);

            while(true) {
                menu();

                System.out.println("Seleccione la operación:");
                //operation = (int) (Math.random() * 5) + 1;
                operation = scN.nextInt();
                dos.writeUTF(operation+"");
                System.out.println("Mensaje enviado");

                if (operation == 5) {
                    break;
                }

                System.out.println("Seleccione el primer número:");
                //num1 = (int) (Math.random() * 100) + 1;
                num1 = scN.nextDouble();
                dos.writeUTF(num1+"");
                System.out.println("Mensaje enviado");

                System.out.println("Seleccione el segundo número:");
                //num2 = (int) (Math.random() * 100) + 1;
                num2 = scN.nextDouble();
                dos.writeUTF(num2+"");
                System.out.println("Mensaje enviado");

                double resultado = Double.parseDouble(dis.readUTF());
                System.out.println("El resultado es: "+resultado);

            }

            System.out.println("Cerrando conexión con el cliente...");
            dos.close();
            dis.close();
            cliente.close();
            System.out.println("Terminando");

        } catch (IOException e) {
            System.err.println("Error al crear el socket.");
            System.out.println(e.getMessage());
        }
    }

    public static void menu() {
        System.out.println("Calculadora:");
        System.out.println("1. Suma");
        System.out.println("2. Resta");
        System.out.println("3. Multiplicación");
        System.out.println("4. División");
        System.out.println("5. Salir");
    }
}
