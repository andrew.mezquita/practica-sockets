package PruebaHilos;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.Socket;
import java.util.Random;

public class ClienteTCP {
    private int id;

    public static void main(String[] args) {
        try {
            Socket cliente = new Socket("localhost", 55000);

            DataInputStream entrada = new DataInputStream(cliente.getInputStream());
            DataOutputStream salida = new DataOutputStream(cliente.getOutputStream());

            Random random = new Random();

            while (true) {
                int operation = random.nextInt(5) + 1;
                System.out.println("El cliente ha elegido "+ getOperationString(operation));
                salida.writeInt(operation);

                if (operation == 5) {
                    break;
                }

                double x = random.nextInt(100) + 1;
                System.out.println("El primer número es "+ x);
                salida.writeDouble(x);

                double y = random.nextInt(100) + 1;
                System.out.println("El segundo número es "+ y);
                salida.writeDouble(y);
            }

            // Cerrar los streams y el socket
            System.out.println("Cerrando conexión con el servidor...");
            entrada.close();
            salida.close();
            cliente.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    private static String getOperationString(int operation) {
        switch (operation) {
            case 1:
                return "Sumar";
            case 2:
                return "Restar";
            case 3:
                return "Multiplicar";
            case 4:
                return "Dividir";
            default:
                return "Operación inválida";
        }
    }
}
