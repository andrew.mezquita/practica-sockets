package PruebaHilos;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Random;
import java.util.concurrent.Semaphore;

public class ServidorTCP {

    public static void main(String[] args) {
        Semaphore semaphore = new Semaphore(5);

        try (ServerSocket servidor = new ServerSocket(55000)) {
            System.out.println("Servidor iniciado. Esperando conexiones...");

            while (true) {
                Socket cliente = servidor.accept();
                System.out.println("Cliente conectado: " + cliente.getInetAddress().getHostAddress());

                semaphore.acquire(); // Adquirir un permiso del semáforo contador

                ClientHandler clientHandler = new ClientHandler(cliente, semaphore);
                Thread clientThread = new Thread(clientHandler);
                clientThread.start();
            }

        } catch (IOException | InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static class ClientHandler implements Runnable {
        private final Socket cliente;
        private final Semaphore semaphore;

        public ClientHandler(Socket cliente, Semaphore semaphore) {
            this.cliente = cliente;
            this.semaphore = semaphore;
        }

        @Override
        public void run() {
            try (
                    DataInputStream entrada = new DataInputStream(cliente.getInputStream());
                    DataOutputStream salida = new DataOutputStream(cliente.getOutputStream())
            ) {
                Random random = new Random();

                semaphore.acquire(); // Esperar a que el cliente anterior termine

                while (true) {
                    int operation = random.nextInt(5) + 1; // Generar aleatoriamente una operación entre 1 y 5

                    if (operation == 5) {
                        System.out.println("El Cliente ha elegido Salir");
                        salida.writeUTF("salir"); // Enviar mensaje de salida al cliente
                        break;
                    }

                    double x = random.nextInt(100) + 1; // Generar aleatoriamente un número entre 1 y 100
                    double y = random.nextInt(100) + 1; // Generar aleatoriamente un número entre 1 y 100

                    System.out.println("El Cliente ha elegido " + getOperationString(operation) +
                            " el número " + x + " y el número " + y);

                    // Simular el tiempo de operación
                    Thread.sleep(3000);

                    // Realizar la operación
                    double resultado = performOperation(operation, x, y);

                    // Enviar el resultado al cliente
                    salida.writeDouble(resultado);
                }

                System.out.println("El Cliente ha salido de la calculadora");
                semaphore.release(); // Liberar el semáforo para permitir que el siguiente cliente avance


            } catch (IOException | InterruptedException e) {
                e.printStackTrace();
            }
        }

        private String getOperationString(int operation) {
            switch (operation) {
                case 1:
                    return "Sumar";
                case 2:
                    return "Restar";
                case 3:
                    return "Multiplicar";
                case 4:
                    return "Dividir";
                default:
                    return "Operación inválida";
            }
        }

        private double performOperation(int operation, double x, double y) {
            switch (operation) {
                case 1:
                    return x + y;
                case 2:
                    return x - y;
                case 3:
                    return x * y;
                case 4:
                    return x / y;
                default:
                    return 0;
            }
        }
    }
}
