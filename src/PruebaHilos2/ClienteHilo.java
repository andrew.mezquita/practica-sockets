package PruebaHilos2;

import java.util.concurrent.Semaphore;

public class ClienteHilo extends Thread{
    private Cliente cliente;
    private int clienteId;
    private int operation;
    private int num1;
    private int num2;
    private Semaphore semaphore;



    public ClienteHilo(Cliente cliente, int clienteId, int operation, int num1, int num2, Semaphore semaphore) {
        this.cliente = cliente;
        this.operation = operation;
        this.clienteId = clienteId;
        this.num1 = num1;
        this.num2 = num2;
        this.semaphore = semaphore;
    }

    @Override
    public void run() {
        try {
            semaphore.acquire();
            //cliente.procesoCalculadora(clienteId, operation, num1, num2);
            semaphore.release();
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

}
