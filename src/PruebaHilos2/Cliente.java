package PruebaHilos2;

import java.io.*;
import java.net.Socket;

public class Cliente {


    public static void main(String[] args) {
        int clienteId = 1, operation, num1, num2;

        //Stream de entrada de datos
        InputStream is = null;
        try {
            Socket cliente = new Socket("localhost", 55000);
            is = cliente.getInputStream();
            DataInputStream dis = new DataInputStream(is);
            //Stream de salida de datos
            OutputStream os = cliente.getOutputStream();
            DataOutputStream dos = new DataOutputStream(os);
            menu();

            System.out.println("El cliente " + clienteId + " ha entrado en la calculadora.");

            operation = (int) (Math.random() * 5) + 1;
            Thread.sleep(2000);
            dos.writeUTF(operation + "");
            System.out.println("El cliente " + clienteId + " ha elegido operación " + operation);

            num1 = (int) (Math.random() * 100) + 1;
            dos.writeUTF(num1 + "");
            num2 = (int) (Math.random() * 100) + 1;
            dos.writeUTF(num2 + "");
            System.out.println("El cliente " + clienteId + " ha elegido los números " + num1 + " y " + num2);
            Thread.sleep(2000);

            if (clienteId == 5 && operation == 5) {
                System.out.println("Cerrando conexión con el cliente...");
                dos.close();
                dis.close();
                cliente.close();
                System.out.println("Terminando");
            }

            System.out.println("La calculadora vuleve a estar disponible");
        } catch (IOException e) {
            throw new RuntimeException(e);
        } catch (InterruptedException e) {
            throw new RuntimeException(e);
        }
    }

    public static void menu() {
        System.out.println("Calculadora:");
        System.out.println("1. Suma");
        System.out.println("2. Resta");
        System.out.println("3. Multiplicación");
        System.out.println("4. División");
        System.out.println("5. Salir");
    }
}
