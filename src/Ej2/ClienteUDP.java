package Ej2;

import java.io.IOException;
import java.net.*;
import java.util.Scanner;

public class ClienteUDP {
    public static void main(String[] args) {

        Scanner scN = new Scanner(System.in);

        final int PUERTO = 55000;
        byte[] sendData;
        byte[] buffer = new byte[1024];

        try {
            InetAddress direction = InetAddress.getByName("localhost");

            DatagramSocket socketUDP;

            socketUDP = new DatagramSocket();

            while (true) {
                menu();
                System.out.println("Seleccione la operación:");
                int operation = scN.nextInt();
                if (operation == 5) {
                    break;
                }

                System.out.println("Seleccione el primer número:");
                double num1 = scN.nextDouble();
                System.out.println("Seleccione el segundo número:");
                double num2 = scN.nextDouble();

                // Crear mensaje para enviar al servidor
                String mensaje = operation + "," + num1 + "," + num2;
                sendData = mensaje.getBytes();

                // Enviar datagrama al servidor
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, direction, 55000);
                socketUDP.send(sendPacket);

                // Recibir respuesta del servidor
                DatagramPacket receivePacket = new DatagramPacket(buffer, buffer.length);
                socketUDP.receive(receivePacket);
                String respuesta = new String(receivePacket.getData(), 0, receivePacket.getLength());

                System.out.println("El resultado es: " + respuesta);
            }
        } catch (SocketException e) {
            throw new RuntimeException(e);
        } catch (UnknownHostException e) {
            throw new RuntimeException(e);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    public static void menu() {
        System.out.println("Calculadora:");
        System.out.println("1. Suma");
        System.out.println("2. Resta");
        System.out.println("3. Multiplicación");
        System.out.println("4. División");
        System.out.println("5. Salir");
    }
}
