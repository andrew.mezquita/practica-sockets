package Ej2;

import PruebaHilos2.Servidor;

import java.io.IOException;
import java.net.DatagramPacket;
import java.net.DatagramSocket;
import java.net.SocketException;
import java.util.logging.Level;
import java.util.logging.Logger;

public class ServidorUDP {
    public static void main(String[] args) {
        final int PUERTO = 55000;
        byte[] buffer = new byte[1024];

        try{
            System.out.println("Iniciando Servidor UDP");
            DatagramSocket socketUDP = new DatagramSocket(PUERTO);

            while (true) {
                // Recibir datagrama
                byte[] receiveData = new byte[1024];
                DatagramPacket peticion = new DatagramPacket(buffer, buffer.length);
                socketUDP.receive(peticion);

                System.out.println("Recibo la información del cliente");
                String mensaje = new String(peticion.getData());
                System.out.println(mensaje);

                // Procesar el mensaje (simulando una operación)
                double resultado = procesarMensaje(mensaje);
                System.out.println("El resultado es "+resultado);

                // Enviar respuesta al cliente
                byte[] sendData = String.valueOf(resultado).getBytes();
                DatagramPacket sendPacket = new DatagramPacket(sendData, sendData.length, peticion.getAddress(), peticion.getPort());
                socketUDP.send(sendPacket);
            }

        } catch (SocketException ex) {
            Logger.getLogger(Servidor.class.getName()).log(Level.SEVERE, null, ex);
        } catch (IOException e) {
            throw new RuntimeException(e);
        }
    }

    private static double procesarMensaje(String mensaje) {
        // Procesar el mensaje recibido
        String[] partes = mensaje.split(",");
        int operation = Integer.parseInt(partes[0]);
        double num1 = Double.parseDouble(partes[1]);
        double num2 = Double.parseDouble(partes[2]);

        // Realizar la operación
        switch (operation) {
            case 1:
                return num1 + num2;
            case 2:
                return num1 - num2;
            case 3:
                return num1 * num2;
            case 4:
                return num1 / num2;
            default:
                return 0;
        }
    }
}
